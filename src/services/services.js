import axios from 'axios';

const axiosInstance = axios.create();

const baseURL = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&nojsoncallback=1&format=json`;


export const postApi = async (searchTxt,pageNum) => {
    return new Promise((resolve, reject) => {
        axiosInstance
            .request({
                url: baseURL + `&text=${searchTxt}&page=${pageNum}&per_page=12`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            .then(response => {
               resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
};