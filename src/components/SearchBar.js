import React from 'react';
import { View, Text, StyleSheet, TextInput, SafeAreaView } from 'react-native';

const SearchBar = ({text, onChangeText}) => {
    return (
        <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={onChangeText}
          value={text}
          placeholder="Search..."
        />
      </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    input: {
        height: 50,
        borderWidth: 1,
        padding: 10,
        width: 280,
        borderRadius: 15,
        marginRight: 10
      },
});

export default SearchBar;
