//import liraries
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Button, FlatList, Dimensions, Image, ActivityIndicator, Platform } from 'react-native';
import SearchBar from '../components/SearchBar';
import { postApi } from '../services/services';
import FastImage from 'react-native-fast-image';

const width = Dimensions.get('screen').width / 3 - 15;

const HomeScreen = () => {
    const [searchTxt, setSearchTxt] = useState('');

    const [imageData, setImageData] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [pageNum, setPageNum] = useState(0);

    useEffect(() => {
        onClickSearch();
    }, [pageNum]);
    const onchangeText = (val) => {
        setSearchTxt(val);
    }
    const onClickSearch = async () => {
        if (!searchTxt) return false;
        await postApi(searchTxt, pageNum)
            .then(data => {
                if (data && data.photos.photo) {
                    data.photos.photo.map((photo) => {
                        const url = getImageUrl(photo);
                        photo.url = url;
                    });
                    setImageData(imageData.concat(data.photos.photo));
                }
            })
            .catch(err => setLoading(false));
    }

    const getImageUrl = (photo) => {
        const url = `https://farm${photo.farm}.static.flickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`;
        return url;
    }

    const Card = ({ item }) => {
        return <View style={styles.card}>
            <FastImage
                source={{ uri: item.url, priority: FastImage.priority.fast, cache: FastImage.cacheControl.immutable }}
                resizeMode={FastImage.resizeMode.contain}
                style={{ height: 150 }}
            />
        </View>
    }

    const onLoadMoreData = async () => {
        setPageNum(pageNum + 1);
    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                <SearchBar onChangeText={onchangeText} value={searchTxt} />
                <View>
                    <Button title='Search' onPress={() => {
                        setImageData([]);
                        setPageNum(1);
                        // onClickSearch();
                    }} color='#fd7833' />
                </View>
            </View>
            <View style={{ marginTop: 50, justifyContent: 'center', alignItems: 'center' }}>
                {isLoading ? <ActivityIndicator size="large" color="#0000ff" /> :
                    <FlatList
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        showsVerticalScrollIndicator={false}
                        numColumns={3}
                        initialNumToRender={10}
                        data={imageData}
                        extraData={imageData}
                        keyExtractor={item => item.id.toString()}
                        renderItem={({ item }) => <Card item={item} />}
                        onEndReached={onLoadMoreData}
                        onEndReachedThreshold={0.5}
                    />}
            </View>
        </View>
    );


};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
        height: '100%',
        width,
        marginHorizontal: 3,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    }
});

export default HomeScreen;
